/**
 * Created by lucast on 14/09/2016.
 */

export {FeatsModuleClient} from "./FeatsModuleClient";
export {AdapterFlags} from "./FeatureExtractor";
export {LocalModuleRequestHandler} from "./LocalModuleRequestHandler";
export {batchProcess, generateSineWave, segmentAudioBuffer} from "./AudioUtilities";