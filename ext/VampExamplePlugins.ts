import {EmscriptenModule} from '../src/Emscripten'

declare function VampExamplePluginsModule(): EmscriptenModule;
export = VampExamplePluginsModule;
