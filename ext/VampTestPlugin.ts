import {EmscriptenModule} from '../src/Emscripten'

declare function VampTestPluginModule(): EmscriptenModule;
export = VampTestPluginModule;
